package pl.codementors.zoo;

/**
 * Created by student on 05.06.17.
 */
public class Wolf extends Mammal implements Carnivarous {

    public void howl() {
        System.out.println("Wolf " + getName() + " howl");
    }

    @Override
    public void eat() {
        System.out.println("Wolf " + getName() + " eat");
    }

    @Override
    public void eatMeat() {
    }

}
