package pl.codementors.zoo;

/**
 * Created by student on 05.06.17.
 */
public class Iguana extends Lizard implements Herbivorous {

    public void hiss() {
        System.out.println("Iguana " + getName() + " hiss");
    }

    public void eat() {
        System.out.println("Parrot " + getName() + " eat");
    }

    @Override
    public void eatPlant() {

    }
}
