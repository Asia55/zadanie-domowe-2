package pl.codementors.zoo;

/**
 * Created by student on 05.06.17.
 */
public class Parrot extends Bird implements Herbivorous {

    public void screech() {
        System.out.println("Parrot " + getName() + " screech");
    }

    @Override
    public void eat() {
        System.out.println("Parrot " + getName() + " eat");
    }

    @Override
    public void eatPlant() {

    }
}
