package pl.codementors.zoo;

import java.io.*;
import java.util.Scanner;

/**
 * Created by student on 05.06.17.
 */
public class ZooMain {

    static void print(Animal[] animals) {
        for (Animal a : animals) {
            System.out.println(a.getClass().getSimpleName() + " " + " " + a.getName());
        }
    }

    static void feed(Animal[] animals) {
        for (Animal f : animals) {
            f.eat();
        }
    }

    static void saveToFile(Animal[] animals, String fileName) {
        try (FileWriter fw = new FileWriter("/tmp/" + fileName);
             BufferedWriter bw = new BufferedWriter(fw);) {

            for (Animal w : animals) {
                bw.write(w.getClass().getSimpleName());
                bw.newLine();
                bw.write(w.getName());
                bw.newLine();
            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public static void howl(Animal[] animals) {
        for (Animal m : animals) {
            if (m instanceof Wolf) {
                ((Wolf) m).howl();
            }
        }
    }

    public static void hiss(Animal[] animals) {
        for (Animal l : animals) {
            if (l instanceof Iguana) {
                ((Iguana) l).hiss();
            }
        }
    }

    public static void screech(Animal[] animals) {
        for (Animal p : animals) {
            if (p instanceof Parrot) {
                ((Parrot) p).screech();
            }
        }
    }

    public static void feedWithMeat(Animal[] animals) {
        for (Animal fee : animals) {
            if (fee instanceof Carnivarous) {
                ((Carnivarous) fee).eatMeat();
            }
        }
    }

    public static void feedWithPlant(Animal[] animals) {
        for (Animal fe : animals) {
            if (fe instanceof Herbivorous) {
                ((Herbivorous) fe).eatPlant();
            }
        }
    }


    public static Animal[] readFromFile(Animal[] animals, String fileName) {
        Animal[] arrayOfAnimals = new Animal[10];
        try (FileReader fr = new FileReader("/tmp/" + fileName);
             Scanner scanner = new Scanner(fr)) {


            for (int i = 0; i < animals.length; i++) {
                String type = scanner.nextLine();
                String name = scanner.nextLine();

                if (type.equalsIgnoreCase("wolf")) {
                    Animal wolf = new Wolf();
                    arrayOfAnimals[i] = wolf;
                } else if (type.equalsIgnoreCase("parrot")) {
                    Animal parrot = new Parrot();
                    arrayOfAnimals[i] = parrot;
                } else if (type.equalsIgnoreCase("iguana")) {
                    Animal iguana = new Iguana();
                    arrayOfAnimals[i] = iguana;
                } else {
                    System.out.println("Zły typ");
                }
            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        return arrayOfAnimals;
    }

    public static void printColors(Animal[] animals) {
        for (Animal r : animals) {
            if (r instanceof Wolf) {
                System.out.println(r.getName());
                System.out.println(((Wolf) r).getFurColor());
            } else if (r instanceof Parrot) {
                System.out.println(r.getName());
                System.out.println(((Parrot) r).getFeatherColor());
            } else if (r instanceof Iguana) {
                System.out.println(r.getName());
                System.out.println(((Iguana) r).getScaleColor());
            } else {
                System.out.println("Zły typ");
            }
        }

    }

    public static void saveToBinaryFile(Animal[] animals, String fileName) {
        try (FileOutputStream fos = new FileOutputStream("/tmp/" + fileName);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            for (Animal z : animals) {
                oos.writeObject(z.getClass().getSimpleName());
                oos.writeObject(z.getName());
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     *
     * @asia Nie potrafię zrobić ostatniego zadania. Zostawiam próby jego rozwiązania.
     */

//    public static Animal[] readFromBinaryFile(Animal[] animals, String fileName) {
//        Animal[] arrayOfAnimals = new Animal[10];
//        try (FileInputStream fis = new FileInputStream("/tmp/" + fileName);
//             ObjectInputStream ois = new ObjectInputStream(fis)) {
//
////            for (int i = 0; i < arrayOfAnimals.length; i++) {
////              fileName = (Animal[arrayOfAnimals.length]) ois.readObject();
////               String type = (String type) ois.readObject();
////               String name = (String<Animal>) ois.readObject();
//
//        }
//        } catch (IOException ex){
//        System.err.println(ex.getMessage());
//        ex.printStackTrace();
//
//    }


    public static void main(String[] args) {
        System.out.println("Hello to the ZOO");


        Scanner inputScanner = new Scanner(System.in);
        boolean run = true;


        while (run) {
            System.out.println("Wybierz polecenie: [add]");
            String command = inputScanner.nextLine();
            System.out.println("Wybrałeś polecenie [" + command + "]");

            switch (command) {
                case "add": {

                    System.out.println("Dodaj zwierzaka.");
                    System.out.println("Ile zwierząt chcesz dodać?");
                    int size = inputScanner.nextInt();
                    inputScanner.skip("\n");

                    Animal[] arrayOfAnimal = new Animal[size];

                    for (int i = 0; i < arrayOfAnimal.length; i++) {
                        System.out.println("Podaj typ");
                        String type = inputScanner.nextLine();
                        type = type.toLowerCase();


                        if (type.equals("wolf")) {

                            Wolf newWolf = new Wolf();
                            System.out.println("Podaj imie");
                            newWolf.setName(inputScanner.nextLine());
                            System.out.println("Podaj kolor");
                            newWolf.setFurColor(inputScanner.nextLine());
                            arrayOfAnimal[i] = newWolf;
                        } else if (type.equals("parrot")) {
                            Parrot newParrot = new Parrot();
                            System.out.println("Podaj imie");
                            newParrot.setName(inputScanner.nextLine());
                            System.out.println("Podaj kolor");
                            newParrot.setFeatherColor(inputScanner.nextLine());
                            arrayOfAnimal[i] = newParrot;
                        } else if (type.equals("iguana")) {
                            Iguana newIguana = new Iguana();
                            System.out.println("Podaj imie");
                            newIguana.setName(inputScanner.nextLine());
                            System.out.println("Podaj kolor");
                            newIguana.setScaleColor(inputScanner.nextLine());
                            arrayOfAnimal[i] = newIguana;
                        } else {
                            System.out.println("Zly typ");
                        }
                    }
                    print(arrayOfAnimal);
                    saveToFile(arrayOfAnimal, "animal.txt");

                }

                break;


            }
        }

    }

}



