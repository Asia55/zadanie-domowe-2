package pl.codementors.zoo;

/**
 * Created by student on 05.06.17.
 */
public abstract class Mammal extends Animal {

    private String furColor;

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }
}
