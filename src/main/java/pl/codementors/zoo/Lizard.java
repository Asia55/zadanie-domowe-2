package pl.codementors.zoo;

/**
 * Created by student on 05.06.17.
 */
public abstract class Lizard extends Animal {

    private String scaleColor;

    public String getScaleColor() {
        return scaleColor;
    }

    public void setScaleColor(String scaleColor) {
        this.scaleColor = scaleColor;
    }
}
