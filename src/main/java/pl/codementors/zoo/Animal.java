package pl.codementors.zoo;

/**
 * Created by student on 05.06.17.
 */
public abstract class Animal {

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract void eat();

    public void printToOutPut() {
        System.out.println("NAME: " + name);
        System.out.println("AGE: " + age);
    }

}
