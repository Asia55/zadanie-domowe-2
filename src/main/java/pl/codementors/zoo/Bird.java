package pl.codementors.zoo;

/**
 * Created by student on 05.06.17.
 */
public abstract class Bird extends Animal {

    private String featherColor;

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }
}
